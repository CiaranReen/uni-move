from django.db import models


class Postcode(models.Model):
    postcode = models.CharField(max_length=7, null=False)
    latitude = models.CharField(max_length=30, null=False)
    longitude = models.CharField(max_length=30, null=False)
