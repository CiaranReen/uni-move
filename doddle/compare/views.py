from django.shortcuts import render
from django.http import HttpResponse
from properties.models import Property


def index(request):
    property = Property.objects.filter()
    return render(request, 'compare/index.html', {
        'property': property
    })