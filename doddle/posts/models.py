from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.CharField(max_length=200)
    created = models.DateTimeField()

    def __str__(self):
        return self.title