from django.shortcuts import render
from postcodes.models import Postcode


def index(request):
    if request.method == 'POST':
        postcode = request.POST['hero-postcode']
        postcode = postcode.replace(" ", "")
        longLat = Postcode.objects.filter(postcode=postcode)
        return render(request, 'maps/index.html', {
            'longLat': longLat,
        })
    else:
        return render(request, 'maps/index.html')