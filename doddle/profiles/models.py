from django.db import models
from django.contrib.auth.models import AbstractUser


UNIVERSITY = (
    ('Leeds Metropolitan University', 'Leeds Metropolitan University'),
    ('Imperial College London ', 'Imperial College London')
)


class UserProfile(AbstractUser):
    university = models.CharField(max_length=200, choices=UNIVERSITY, null=True)
    city = models.CharField(max_length=30, null=True)
    is_agent = models.CharField(max_length=5, default='false')
