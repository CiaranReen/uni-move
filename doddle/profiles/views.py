from django.shortcuts import render
from django.forms import forms, ModelForm
from django.contrib.auth.models import User
from .models import UserProfile
from django.http import HttpResponseRedirect
from django.contrib.auth.models import models


class UserForm(ModelForm):

    class Meta:
        model = UserProfile
        fields = ('username', 'first_name', 'last_name', 'email', 'city', 'university')


def index(request):
    if request.user.is_authenticated:
        User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
        if request.method == 'POST':
            form = UserForm(request.POST, instance=UserProfile.objects.get(pk=request.user.id))
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/')
        else:
            form = UserForm(instance=request.user)
        return render(request, 'profiles/index.html', {
            'form': form,
            'profile': request.user
        })