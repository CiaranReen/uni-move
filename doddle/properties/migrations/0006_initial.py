# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Property'
        db.create_table('properties_property', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('line_one', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('line_two', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('agent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agents.Agent'])),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=10000)),
            ('price', self.gf('django.db.models.fields.IntegerField')(max_length=20)),
            ('approved', self.gf('django.db.models.fields.CharField')(max_length=1, default='n')),
            ('featured', self.gf('django.db.models.fields.CharField')(max_length=1, default='n')),
            ('date_available', self.gf('django.db.models.fields.DateField')()),
            ('no_of_bedrooms', self.gf('django.db.models.fields.CharField')(max_length=2, default=1)),
            ('no_of_bathrooms', self.gf('django.db.models.fields.CharField')(max_length=1, default=1)),
            ('bills_included', self.gf('django.db.models.fields.CharField')(max_length=1, default='n')),
            ('is_furnished', self.gf('django.db.models.fields.CharField')(max_length=1, default='n')),
        ))
        db.send_create_signal('properties', ['Property'])

        # Adding model 'PropertyImage'
        db.create_table('properties_propertyimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('property_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['properties.Property'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('is_main', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('properties', ['PropertyImage'])

        # Adding model 'Review'
        db.create_table('properties_review', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['properties.Property'])),
            ('star_rating', self.gf('django.db.models.fields.IntegerField')(max_length=1, default=1)),
            ('comment', self.gf('django.db.models.fields.TextField')(max_length=1000)),
            ('approved', self.gf('django.db.models.fields.CharField')(max_length=1, default='n')),
        ))
        db.send_create_signal('properties', ['Review'])

        # Adding model 'SavedProperty'
        db.create_table('properties_savedproperty', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profiles.UserProfile'])),
            ('property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['properties.Property'])),
        ))
        db.send_create_signal('properties', ['SavedProperty'])


    def backwards(self, orm):
        # Deleting model 'Property'
        db.delete_table('properties_property')

        # Deleting model 'PropertyImage'
        db.delete_table('properties_propertyimage')

        # Deleting model 'Review'
        db.delete_table('properties_review')

        # Deleting model 'SavedProperty'
        db.delete_table('properties_savedproperty')


    models = {
        'agents.agent': {
            'Meta': {'object_name': 'Agent'},
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'facebook': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '55'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '55'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'profiles.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Group']", 'related_name': "'user_set'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_agent': ('django.db.models.fields.CharField', [], {'max_length': '5', 'default': "'false'"}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'related_name': "'user_set'", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'properties.property': {
            'Meta': {'object_name': 'Property'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['agents.Agent']"}),
            'approved': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'n'"}),
            'bills_included': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'n'"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'date_available': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'featured': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'n'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_furnished': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'n'"}),
            'line_one': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'line_two': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'no_of_bathrooms': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': '1'}),
            'no_of_bedrooms': ('django.db.models.fields.CharField', [], {'max_length': '2', 'default': '1'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'price': ('django.db.models.fields.IntegerField', [], {'max_length': '20'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'properties.propertyimage': {
            'Meta': {'object_name': 'PropertyImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_main': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'property_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['properties.Property']"})
        },
        'properties.review': {
            'Meta': {'object_name': 'Review'},
            'approved': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'n'"}),
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['properties.Property']"}),
            'star_rating': ('django.db.models.fields.IntegerField', [], {'max_length': '1', 'default': '1'})
        },
        'properties.savedproperty': {
            'Meta': {'object_name': 'SavedProperty'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['properties.Property']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['profiles.UserProfile']"})
        }
    }

    complete_apps = ['properties']