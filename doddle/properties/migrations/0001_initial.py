# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Property'
        db.create_table('properties_property', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('line_one', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('line_two', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('agent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agents.Agent'])),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=10000)),
            ('price', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('approved', self.gf('django.db.models.fields.CharField')(default='n', max_length=1)),
            ('featured', self.gf('django.db.models.fields.CharField')(default='n', max_length=1)),
            ('date_available', self.gf('django.db.models.fields.DateField')()),
            ('no_of_bedrooms', self.gf('django.db.models.fields.CharField')(default=1, max_length=2)),
            ('no_of_bathrooms', self.gf('django.db.models.fields.CharField')(default=1, max_length=1)),
            ('bills_included', self.gf('django.db.models.fields.CharField')(default='n', max_length=1)),
            ('is_furnished', self.gf('django.db.models.fields.CharField')(default='n', max_length=1)),
        ))
        db.send_create_signal('properties', ['Property'])

        # Adding model 'PropertyImage'
        db.create_table('properties_propertyimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('property_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['properties.Property'])),
            ('image', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('is_main', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('properties', ['PropertyImage'])

        # Adding model 'Review'
        db.create_table('properties_review', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['properties.Property'])),
            ('star_rating', self.gf('django.db.models.fields.IntegerField')(default=1, max_length=1)),
            ('comment', self.gf('django.db.models.fields.TextField')(max_length=1000)),
            ('approved', self.gf('django.db.models.fields.CharField')(default='n', max_length=1)),
        ))
        db.send_create_signal('properties', ['Review'])

        # Adding model 'SavedProperty'
        db.create_table('properties_savedproperty', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['properties.Property'])),
        ))
        db.send_create_signal('properties', ['SavedProperty'])


    def backwards(self, orm):
        # Deleting model 'Property'
        db.delete_table('properties_property')

        # Deleting model 'PropertyImage'
        db.delete_table('properties_propertyimage')

        # Deleting model 'Review'
        db.delete_table('properties_review')

        # Deleting model 'SavedProperty'
        db.delete_table('properties_savedproperty')


    models = {
        'agents.agent': {
            'Meta': {'object_name': 'Agent'},
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'symmetrical': 'False', 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False', 'related_name': "'user_set'"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'properties.property': {
            'Meta': {'object_name': 'Property'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['agents.Agent']"}),
            'approved': ('django.db.models.fields.CharField', [], {'default': "'n'", 'max_length': '1'}),
            'bills_included': ('django.db.models.fields.CharField', [], {'default': "'n'", 'max_length': '1'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'date_available': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'featured': ('django.db.models.fields.CharField', [], {'default': "'n'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_furnished': ('django.db.models.fields.CharField', [], {'default': "'n'", 'max_length': '1'}),
            'line_one': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'line_two': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'no_of_bathrooms': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'no_of_bedrooms': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'properties.propertyimage': {
            'Meta': {'object_name': 'PropertyImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'is_main': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'property_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['properties.Property']"})
        },
        'properties.review': {
            'Meta': {'object_name': 'Review'},
            'approved': ('django.db.models.fields.CharField', [], {'default': "'n'", 'max_length': '1'}),
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['properties.Property']"}),
            'star_rating': ('django.db.models.fields.IntegerField', [], {'default': '1', 'max_length': '1'})
        },
        'properties.savedproperty': {
            'Meta': {'object_name': 'SavedProperty'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['properties.Property']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }

    complete_apps = ['properties']