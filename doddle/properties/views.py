from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.forms import ModelForm
from django.db.models import Sum, Count
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from .models import Property, Review, PropertyImage, SavedProperty
from agents.models import Agent
from postcodes.models import Postcode
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os


#Model Forms
class AddPropertyForm(ModelForm):
    class Meta:
        model = Property
        fields = ['line_one', 'line_two', 'city', 'postcode', 'agent']


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        exclude = ['approved']


#Views
def index(request):
    approved = 'y'

    if 'results' not in request.POST:
        number_of_pages = 10
    else:
        number_of_pages = request.POST['results']

    if request.method == 'POST' and 'hero-postcode' in request.POST:
        postcode = request.POST['hero-postcode']
        property = Property.objects.filter(postcode=postcode, approved=approved)
        paginator = Paginator(property, 2)
        count = property.count()

        page = request.GET.get('page')
        try:
            property = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            property = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            property = paginator.page(paginator.num_pages)
        return render(request, 'properties/index.html', {
            'count': count,
            'property': property,
        })
    elif request.method == 'POST':
        location = request.POST['location']
        min_price = request.POST['min-price']
        max_price = request.POST['max-price']
        min_bedrooms = request.POST['min-bedrooms']
        max_bedrooms = request.POST['max-bedrooms']
        type = request.POST['type']
        property = Property.objects.filter(city__contains=location, price__gte=min_price, price__lte=max_price,
                                           no_of_bedrooms__gte=min_bedrooms, no_of_bedrooms__lte=max_bedrooms,
                                           type__contains=type, approved=approved)
        paginator = Paginator(property, number_of_pages)
        page = request.GET.get('page')
        try:
            property = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            property = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            property = paginator.page(paginator.num_pages)
        #count = property.count()
        return render(request, 'properties/index.html', {
            #'count': count,
            'property': property,
        })
    else:
        property = Property.objects.filter(approved=approved)
        review_object = Review.objects.filter(property_id=property.values('id')).values('property').annotate(Count('property'))
        score = Review.objects.values('property_id').annotate(total=Sum('star_rating'))
        count = property.count()

        #Algo for getting the average rating per property
        average_rating = []
        for prop in property:
            for review in prop.review_set.all():
                average_rating.append(review.star_rating)

        paginator = Paginator(property, 10)
        page = request.GET.get('page')
        try:
            property = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            property = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            property = paginator.page(paginator.num_pages)

        return render(request, 'properties/index.html', {
            'count': count,
            'review': review_object,
            'score': score,
            'average_rating': average_rating,
            'property': property
        })


def view(request, property_id):
    try:
        prop = Property.objects.get(pk=property_id)
        image = PropertyImage.objects.filter(property_id=property_id)
        agent = Agent.objects.get(pk=prop.agent_id)
        postcode = prop.postcode.replace(" ", "")
        postcode = Postcode.objects.get(postcode=postcode)

        #Fetch recommended property based on current property
        #Exclude id to avoid duplication of main property
        recommended = Property.objects.filter(line_two=prop.line_two, type=prop.type, no_of_bedrooms=prop.no_of_bedrooms)\
            .exclude(id=prop.id)
    except Property.DoesNotExist:
        raise Http404
    return render(request, 'properties/view.html', {
        'postcode': postcode,
        'recommended': recommended,
        'image': image,
        'agent': agent,
        'property': prop
    })


def agent(request, agent_id):
    try:
        property = Property.objects.filter(agent_id=agent_id)
        agent = Agent.objects.get(id=agent_id)
    except Agent.DoesNotExist:
        raise Http404
    return render(request, 'properties/agent.html', {
        'property': property,
        'agent': agent,
        'count': property.count(),
    })


def review(request, property_id):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        form.property_id = property_id
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/properties/')
    else:
        form = ReviewForm(initial={'property': property_id})
        property = Property.objects.get(pk=property_id)
        return render(request, 'properties/review.html', {
            'form': form,
            'propertyObject': property,
        })


def addproperty(request):
    if request.method == 'POST':
        form = AddPropertyForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = AddPropertyForm()
        return render(request, 'properties/add-property.html', {
            'form': form,
        })


def enquire(request, property_id):
    if request.method == 'POST':
        sender = request.POST['sender']
        recipients = request.POST['recipient']
        subject = request.POST['subject']
        property = request.POST['property']
        message = request.POST['message']
        send_mail(subject, property, message, sender, recipients)
        return HttpResponseRedirect('/thanks/')
    else:
        property = Property.objects.get(pk=property_id)
        agent = Agent.objects.get(pk=property.agent_id)
        image = PropertyImage.objects.filter(pk=property.id)
        return render(request, 'properties/enquire.html', {
            'property': property,
            'image': image,
            'agent': agent,
        })


@csrf_exempt
def save_property(request):
    if request.POST and request.is_ajax:
        property_id = request.POST.get('propertyId')
        user_id = request.POST.get('userId')
        saved_property = SavedProperty(property_id=property_id, user_id=user_id)
        s = saved_property.save()
        return HttpResponse(s)


@csrf_exempt
def remove_saved_property(request):
    if request.POST and request.is_ajax:
        property_id = request.POST.get('propertyId')
        user_id = request.POST.get('userId')
        saved_property = SavedProperty.objects.get(property_id=property_id, user_id=user_id)
        s = saved_property.delete()
        return HttpResponse(s)


@csrf_exempt
def remove_property(request):
    if request.POST and request.is_ajax:
        property_id = request.POST.get('propertyId')

        #Remove all the related images for the property due to FK
        propertyImage = PropertyImage.objects.filter(id=property_id)
        s = propertyImage.delete()

        #Remove the property from the db
        property = Property.objects.filter(id=property_id)
        s = property.delete()
        return HttpResponse(s)


@csrf_exempt
def remove_image(request):
    if request.POST and request.is_ajax:
        image_id = request.POST.get('imageId')
        image = PropertyImage.objects.get(id=image_id)
        s = image.delete()
        os.remove('/home/ciaran/PycharmProjects/doddle_project/doddle/properties/'+image.image.url)
        return HttpResponse(s)