from django.conf.urls import patterns, url
from properties import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^add-property/$', views.addproperty, name='add-property'),
    url(r'^(?P<property_id>\d+)/view/$', views.view, name='view'),
    url(r'^(?P<property_id>\d+)/review/$', views.review, name='review'),
    url(r'^(?P<property_id>\d+)/enquire/$', views.enquire, name='enquire'),
    url(r'^(?P<agent_id>\d+)/agent/$', views.agent, name='agent'),

    #AJAX urls
    url(r'^save-property/$', views.save_property, name='save-property'),
    url(r'^remove-property/$', views.remove_property, name='remove-property'),
    url(r'^remove-saved-property/$', views.remove_saved_property, name='remove-saved-property'),
    url(r'^remove-image/$', views.remove_image, name='remove-image'),
)