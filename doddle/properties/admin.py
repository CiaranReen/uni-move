from django.contrib import admin
from .models import Property, PropertyImage, Review


class PropertyAdmin(admin.ModelAdmin):
    list_display = ('line_one', 'line_two', 'city', 'postcode', 'approved')
    list_filter = ['approved']
    search_fields = ['line_one', 'line_two', 'city', 'postcode']

admin.site.register(Property, PropertyAdmin)
admin.site.register(PropertyImage)
admin.site.register(Review)

