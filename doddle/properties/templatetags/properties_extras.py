from django import template

register = template.Library()

@register.filter
def add(dict, key):
    return dict[key]