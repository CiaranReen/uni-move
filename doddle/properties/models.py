from django.db import models
from agents.models import Agent
from profiles.models import UserProfile
import os


def get_upload_path(instance, filename):
    return os.path.join("static/properties/images/%d/" % instance.property_id, filename)


#Set approved constants
YES = 'y'
NO = 'n'

#Set the array to be displayed in dropdown
YES_NO = (
    (YES, 'Yes'),
    (NO, 'No')
)


class Property(models.Model):
    line_one = models.CharField(max_length=100)
    line_two = models.CharField(max_length=100)
    city = models.CharField(max_length=20)
    postcode = models.CharField(max_length=8)

    HOUSE = 'house'
    FLAT = 'flat/apartment'

    TYPE = (
        (HOUSE, 'House'),
        (FLAT, 'Flat/Apartment')
    )

    type = models.CharField(max_length=20, choices=TYPE)

    agent = models.ForeignKey(Agent)
    description = models.TextField(max_length=10000)
    price = models.IntegerField(max_length=20)

    approved = models.CharField(max_length=1, choices=YES_NO, default=NO)
    featured = models.CharField(max_length=1, choices=YES_NO, default=NO)
    date_available = models.DateField()
    no_of_bedrooms = models.CharField(max_length=2, default=1)
    no_of_bathrooms = models.CharField(max_length=1, default=1)
    bills_included = models.CharField(max_length=1, choices=YES_NO, default=NO)
    is_furnished = models.CharField(max_length=1, choices=YES_NO, default=NO)

    def __str__(self):
        return self.line_one

    def is_approved(self):
        return self.approved
    is_approved.admin_order_field = 'approved'
    is_approved.boolean = True
    is_approved.short_description = 'Approved?'


class PropertyImage(models.Model):
    property = models.ForeignKey(Property)
    image = models.ImageField(upload_to=get_upload_path)
    is_main = models.CharField(max_length=1, choices=YES_NO)


class Review(models.Model):
    property = models.ForeignKey(Property)

    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5

    STAR_RATING = (
        (ONE, '1'),
        (TWO, '2'),
        (THREE, '3'),
        (FOUR, '4'),
        (FIVE, '5')
    )

    star_rating = models.IntegerField(max_length=1, choices=STAR_RATING, default=ONE)
    comment = models.TextField(max_length=1000)

    approved = models.CharField(max_length=1, choices=YES_NO, default=NO)

    def __str__(self):
        return self.property.line_one


class SavedProperty(models.Model):
    user = models.ForeignKey(UserProfile)
    property = models.ForeignKey(Property)