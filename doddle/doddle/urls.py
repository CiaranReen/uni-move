from django.conf.urls import patterns, include, url
from django.contrib import admin
from registration.backends.default.views import RegistrationView
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', include('home.urls')),
    url(r'^about/', include('django.contrib.flatpages.urls')),
    url(r'^faq/', include('django.contrib.flatpages.urls')),
    url(r'^compare/', include('compare.urls')),
    url(r'^agents/', include('agents.urls')),
    url(r'^properties/', include('properties.urls'), name='properties'),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^my-profile/', include('profiles.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^accounts/password/change/$', 'django.contrib.auth.views.password_change'),
    url(r'^accounts/password/password-changed/$', 'django.contrib.auth.views.password_change_done'),
    url(r'^accounts/password/reset/$', 'django.contrib.auth.views.password_reset'),
    url(r'^accounts/password/password-reset/$', 'django.contrib.auth.views.password_reset_complete'),
    url(r'^accounts/register/$', RegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/register/complete/$', TemplateView.as_view(template_name='registration/registration_complete.html'), name='registration_complete'),
    url(r'accounts/', include('django.contrib.auth.urls')),
    url(r'^unimoveadmin/', include(admin.site.urls)),
)
