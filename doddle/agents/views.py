from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from properties.models import Property, PropertyImage
from django.forms import ModelForm


#Model Forms
class PropertyForm(ModelForm):
    class Meta:
        model = Property
        fields = ('line_one', 'line_two', 'city', 'postcode', 'price', 'type', 'description', 'date_available',
                  'no_of_bedrooms', 'no_of_bathrooms', 'bills_included', 'is_furnished')


class ImageForm(ModelForm):
    class Meta:
        model = PropertyImage
        fields = ('property', 'is_main', 'image')


def index(request):
    return HttpResponseRedirect('/')


def manage(request, agent_id):
    property = Property.objects.filter(agent_id=agent_id)
    return render(request, 'agents/manage.html', {
        'property': property
    })


def add(request, agent_id):
    if request.method == 'POST':
        form = PropertyForm(request.POST)
        if form.is_valid():
            agent = form.save(commit=False)
            agent.agent_id = agent_id
            agent.save()

            #Save the image in the property_image table
            property = Property.objects.latest('id')
            main_image = request.POST.get('main-image')
            image = PropertyImage(property_id=property.id, image=main_image, is_main='y')
            image.save()
            return HttpResponseRedirect('/agents/'+agent_id+'/manage/')
    else:
        form = PropertyForm()
        return render(request, 'agents/add.html', {
            'form': form,
        })


def edit(request, agent_id, property_id):
    if request.method == 'POST':
        form = PropertyForm(request.POST, instance=Property.objects.get(pk=property_id))
        if form.is_valid():
            agent = form.save(commit=False)
            agent.agent_id = agent_id
            agent.save()
            return HttpResponseRedirect('/agents/'+agent_id+'/manage/')
    else:
        property = Property.objects.get(pk=property_id)
        form = PropertyForm(instance=property)
        return render(request, 'agents/add.html', {
            'form': form,
        })


def images(request, agent_id, property_id):
    if request.method == 'POST':
        form = PropertyForm(request.POST)
        if form.is_valid():
            agent = form.save(commit=False)
            agent.agent_id = agent_id
            agent.save()
            return HttpResponseRedirect('/agents/'+agent_id+'/manage/')
    else:
        property_image = PropertyImage.objects.filter(property_id=property_id)
        return render(request, 'agents/images.html', {
            'property_image': property_image,
        })


def addimage(request, agent_id):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/agents/'+agent_id+'/manage/')
    else:
        form = ImageForm()
        return render(request, 'agents/add-image.html', {
            'form': form,
        })