from django.conf.urls import patterns, url
from agents import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<agent_id>\d+)/manage/$', views.manage, name='manage'),
    url(r'^(?P<agent_id>\d+)/add/$', views.add, name='add'),
    url(r'^(?P<agent_id>\d+)/(?P<property_id>\d+)/edit/$', views.edit, name='edit'),
    url(r'^(?P<agent_id>\d+)/(?P<property_id>\d+)/images/$', views.images, name='images'),
    url(r'^(?P<agent_id>\d+)/add-image/$', views.addimage, name='add-image'),
)