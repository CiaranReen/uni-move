# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Agent'
        db.create_table('agents_agent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_number', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('agents', ['Agent'])


    def backwards(self, orm):
        # Deleting model 'Agent'
        db.delete_table('agents_agent')


    models = {
        'agents.agent': {
            'Meta': {'object_name': 'Agent'},
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['agents']