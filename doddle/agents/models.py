from django.db import models


class Agent(models.Model):
    name = models.CharField(max_length=50)
    contact_number = models.CharField(max_length=15)
    logo = models.CharField(max_length=55)
    facebook = models.CharField(max_length=100)
    twitter = models.CharField(max_length=55)
    description = models.TextField()

    def __str__(self):
        return self.name