from django.shortcuts import render
from properties.models import Property, PropertyImage


def index(request):
    featured_properties = Property.objects.filter(featured='y')
    return render(request, 'home/index.html', {
        'featured': featured_properties
    })


def team(request):
    return render(request, 'home/team.html')


def about(request):
    return render(request, 'home/about.html')


def faq(request):
    return render(request, 'home/faq.html')